section .text

%define READ_SYSCALL_NUMBER 0
%define WRITE_SYSCALL_NUMBER 1
%define EXIT_SYSCALL_NUMBER 60
%define FD_STDIN 0
%define FD_STDOUT  1
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT_SYSCALL_NUMBER
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.string_loop:
    cmp byte[rax+rdi], 0
    je .end_of_string
    inc rax
    jmp .string_loop
.end_of_string:
	ret


print_string:
    push rdi
    call string_length
    mov rdx, rax        
    pop rsi
    mov rax, WRITE_SYSCALL_NUMBER           
    mov rdi, FD_STDOUT           
    syscall
    ret



; Принимает код символа и выводит его в stdout
print_newline:
    mov rdi, 0xA
    
print_char:
    sub rsp, 1
    mov byte [rsp], dil
    mov rsi, rsp    
    mov rax, WRITE_SYSCALL_NUMBER  
    mov rdi, FD_STDOUT     
    mov rdx, 1 ; length     
    syscall
    add rsp, 1
    ret




; Выводит знаковое 8-байтовое число в десятичном формате 
; Функция выводит знаковое 8-байтовое число в десятичном формате
; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi    
    jns .is_positive      
    push rdi
    mov rax, WRITE_SYSCALL_NUMBER        
    mov rdi, FD_STDOUT            
    mov rdx, 1 ; len           
    sub rsp, 1            
    mov byte [rsp], '-'   
    mov rsi, rsp          

    syscall               

    add rsp, 1           
    pop rdi
    neg rdi              
.is_positive:
    
    
; Переводит строку (выводит символ с кодом 0xA)   



; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push rbx
    push rsi
    xor rax,rax
    sub rsp, 20

    lea rsi, [rsp + 19];we do this to write down division remainders from the end
    mov rax, rdi 
    xor rcx, rcx
.loop:    
    xor rdx, rdx 
    mov rbx, 10 ; base of system
    div rbx 
    add dl, '0'; conversion to ASCII symbol
    dec rsi 
    mov [rsi], dl
    inc rcx
    test rax, rax
    je .end_loop

    jmp .loop

.end_loop:

    mov rax, 1      
    mov rdi, 1     
    mov rdx, rcx      
    syscall

    add rsp, 20
    pop rsi
    pop rbx
    ret


  





; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе

string_equals:
.loop:
    mov al, [rdi]
    cmp al, [rsi]
    jne .not_equals
    
    test al, al
    je .equals   
    inc rdi
    inc rsi
    jmp .loop
    
  
.not_equals:
    xor rax, rax
    ret

.equals:
    mov rax, 1
    ret
 





; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:          
    dec rsp   
    mov rax, READ_SYSCALL_NUMBER     
    mov rdi, FD_STDIN       
    mov rsi, rsp   
    mov rdx, 1 ;len         
    syscall            
    test rax, rax
    jz .eof
    mov al, byte[rsp]
    inc rsp     
    ret
.eof:
    xor rax, rax    
    inc rsp     
    ret
	

	
; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push rbx
    push r12
    push r13
    push r14
    
    mov r12, rbx
    mov r13, rdi
    mov r14, rsi

.loope:
    cmp r12, r14
    jae .error 
    call read_char
    test rax, rax
    je .success
    
    cmp rax, ` ` 
    je .tabs
    cmp rax, `\t`
    je .tabs
    cmp rax, `\n` 
    je .tabs
    
    mov [r13 + r12], rax
    inc r12
    
    jmp .loope
    
    
.success:
    mov byte [r13 + r12], 0
    mov rax, r13
    mov rdx, r12
    pop r14
    pop r13
    pop r12
    pop rbx
    ret
       

.error:
    xor rax, rax
    pop r14
    pop r13
    pop r12
    pop rbx 
    ret
    
.tabs:
    test r12, r12
    jne .success
    jmp .loope




 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    push rbx
    xor rax, rax
    xor rbx, rbx
    xor rdx, rdx
    test rdi, rdi
    jz .done

.loop:

    movzx rbx, byte [rdi + rdx] 
    test rbx, rbx           
    jz .done
    sub rbx, '0' ;subtracts to compare with numbers
    cmp rbx, 9
    ja .done
    imul rax, rax, 10
    add rax, rbx  
    inc rdx  
    jmp .loop

.done:
    test rdx, rdx
    jz .clear_rdx
    pop rbx
    ret
.clear_rdx
    xor rax, rax
    xor rdx, rdx
    pop rbx
    ret   


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    push rdi  
    mov al, [rdi]  
    cmp al, '-'  
    jne .positive  
    inc rdi  
    call parse_uint  
    test rdx, rdx
    jz .clear_rdx
    neg rax  
    inc rdx  
    pop rdi  
    ret

.positive:
    call parse_uint  
    test rdx, rdx  
    jnz .done  

.clear_rdx:
    xor rax, rax
    xor rdx, rdx

.done:
    pop rdi 
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rcx, rcx 
.loop:
    mov al, [rdi + rcx]
    test al, al
    jz .done
    cmp rcx, rdx
    jae .buffer_too_small
    mov [rsi + rcx], al
    inc rcx
    jmp .loop

.buffer_too_small:
    xor rax, rax
    xor rcx, rcx
    jmp .restore_and_return

.done:
    mov byte [rsi + rcx], 0
    mov rax, rcx

.restore_and_return:
    ret
